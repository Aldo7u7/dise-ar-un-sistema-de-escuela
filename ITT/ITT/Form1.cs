﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void profesores_Click(object sender, EventArgs e)
        {
            Form maestros = new docentes();
            maestros.Show();
        }

        private void Alumnos_Click(object sender, EventArgs e)
        {
            Form estudiantes = new alumnos();
            estudiantes.Show();
        }

        private void materias_Click(object sender, EventArgs e)
        {
            Form asignaturas = new materias();
            asignaturas.Show();
        }
    }
}
