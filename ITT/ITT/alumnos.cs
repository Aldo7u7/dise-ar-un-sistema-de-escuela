﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ITT
{
    public partial class alumnos : Form
    {
        DataAcces.DataSet1TableAdapters.materiasTableAdapter tamaterias = new DataAcces.DataSet1TableAdapters.materiasTableAdapter();
        public alumnos()
        {
            InitializeComponent();
        }

        private void alumnos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.materias' table. You can move, or remove it, as needed.
            this.materiasTableAdapter.Fill(this.dataSet1.materias);
            // TODO: This line of code loads data into the 'dataSet1.estudiantes' table. You can move, or remove it, as needed.
            this.estudiantesTableAdapter.Fill(this.dataSet1.estudiantes);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tamaterias.Insert(textBox2.Text);
            int.Parse(textBox1.Text);
            tamaterias.Insert(textBox1.Text);
            this.estudiantesTableAdapter.Fill(this.dataSet1.estudiantes);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tamaterias.DeleteQuery((int)comboBox1.SelectedValue);
            this.estudiantesTableAdapter.Fill(this.dataSet1.estudiantes);
        }
    }
}
